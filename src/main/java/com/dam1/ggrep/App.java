package com.dam1.ggrep;
import java.io.*;
import java.util.Vector;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class App 
{
	private static FileReader fr = null;
	public static void main(String[] args)
	{
		JFrame jf = new JFrame("GREP GRAFICO");
		JPanel jp = new JPanel(new GridLayout(3,2));
		JLabel jl1 = new JLabel("Patrón de búsqueda:");
		JLabel jl2 = new JLabel("Ruta al fichero:");
		final JTextField jtf1 = new JTextField();
		final JTextField jtf2 = new JTextField();
		final JButton jb = new JButton("Muestra líneas");
		final JTextArea jta = new JTextArea();
		jf.add(jp);	// añado panel a la ventana
		jp.add(jl1); jp.add(jtf1); jp.add(jl2);jp.add(jtf2);jp.add(jb);jp.add(jta);

		jf.setVisible(true);
		jf.pack();
		jf.setLocationRelativeTo(null);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		int cont = 1;
/*		if(args.length < 2)
		{
			System.out.println("Debes especificar el patrón de búsqueda y el nombre del fichero");
			System.exit(0);
		}
		// args[0] es el patrón de búsqueda, args[1] el nombre del fichero*/
		// Respuesta al evento clic sobre el botón (Listener "ActionListener", método actionPerformed)
		jb.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					jta.setText("");
					File f = new File(jtf2.getText());
					try
					{
						fr = new FileReader(f);
					}
					catch(FileNotFoundException FNE)
					{
						System.err.println("Se ha liado parda");
					}
					final BufferedReader br = new BufferedReader(fr);
					Vector<String> v = obtieneLineas(jtf1.getText(),br);
					for ( String s: v)
						jta.append(s + "\n");
				}
			});

	}

	public static Vector<String> obtieneLineas(String patron,BufferedReader br)
	{
		String linea;
		Vector<String> v = new Vector<String>();
		try
		{
			while((linea = br.readLine()) != null)
				if(linea.contains(patron))	// si la línea contiene el patrón
				    //System.out.println(linea);
				    v.add(linea);
            		if (br != null) { br.close(); fr.close(); }
		}
		catch(IOException e)
		{
			System.out.println("IOException");
		}
		return v;
	}
}
